#!/bin/bash

PIPELINE_URL=${1?"Please provide pipeline URL"}

PIPELINE_ID=${PIPELINE_URL##*/}

_extract_pref=${PIPELINE_URL%/-/pipelines/*}
PROJECT_PATH=${_extract_pref##https://gitlab.com/}

ARTIFACT_URI=$(glab ci get -R ${PROJECT_PATH} -F json -p ${PIPELINE_ID} | jq -r '.jobs[] | select(.name == "transform:t2g") | { "job_id": .id, "project_id": .pipeline.project_id, "pipeline_id": .pipeline.id } | "/projects/\(.project_id)/jobs/\(.job_id)/artifacts/gl-container-scanning-report.json"')

glab api ${ARTIFACT_URI} 