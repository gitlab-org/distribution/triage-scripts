#!/bin/bash

CVE_TERSE_FORMAT='.cves | join(" ")'
CVE_ONLY_FORMAT='"* \(.name) \(.details)"'
CVE_UNGROUPED_FORMAT="."
LONG_FORMAT='"| \(.name) | \(.version) |  \(.severity)| \(.description)| \(.details)|"'
LONG_HEADER="| name | version | severity | description | details|
|--|--|--|--|--|"

FORMAT_STR=""
HEADER_STR=""
GROUP_BY=""

SCOPE=${SCOPE:-"all"}

grouped_by_details(){
  local selector=${1:-""}
  shift
  jq -r '[.vulnerabilities[] | '"$selector"' { "description": .description, "severity": .severity, "name": .location.dependency.package.name, "version": .location.dependency.version, "details": (.identifiers | map("[\(.name)](\(.url))" ) | join(", ")) }]  | group_by(.details) | .[] | { "details": .[0].details, "packages": (. | map(.name) | unique | join(", "))} | "* [ ]  \(.details) \(.packages)" ' $@
}

grouped_by_packages(){
  local selector=${1:-""}
  shift
  jq -r '[.vulnerabilities[] | '"$selector"' { "description": .description, "severity": .severity, "name": .location.dependency.package.name, "version": .location.dependency.version, "details": (.identifiers | map("[\(.name)](\(.url))" ) | join(", ")) }]  | group_by(.name) | .[] | { "details": (. | map(.details)| join(", ")), "package": .[0].name } | "* [ ] \(.package) \(.details)" ' $@
}


ungrouped(){
  local selector=${1:-""}
  shift
  echo "${HEADER_STR}"
  jq -r '.vulnerabilities[] | '"$selector"' { "description": .description, "severity": .severity, "name": .location.dependency.package.name, "version": .location.dependency.version, "details": (.identifiers | map("[\(.name)](\(.url))" ) | join(", ")), "cves": (.identifiers | map("\(.name)")) } | '"${FORMAT_STR}"' ' $@ 
}

FORMAT=${FORMAT:-"long"}

_criterion=""
for s in ${SCOPE}
do
  _new_criterion=""
  case "$s" in
    "all")
      _criterion=""
      ;;
    "go")
      _new_criterion='(.cve|endswith(":go"))'
      ;;
    "not_go")
      _new_criterion='(.location.dependency.package.name != "go")'
      ;;
    "application")
      _new_criterion='(.cve|endswith(":application"))'
      ;;
    "gem")
      _new_criterion='(.cve|endswith(":gem"))'
      ;;
    "os")
      _new_criterion='(.cve|endswith(":os"))'
      ;;
    "not_os")
      _new_criterion='(.location.dependency.package.name != "os")'
      ;;
    "python")
      _new_criterion='(.cve|endswith(":python"))'
      ;;
  esac
  if [ -n "${_new_criterion}" ]; then
    if [ -z "${_criterion}" ]; then
      _criterion="${_new_criterion}"
    else
      _criterion="${_criterion} or ${_new_criterion}"
    fi
  fi
done
if [ -n "${_criterion}" ]; then
  SELECTOR_STR="select(${_criterion}) | "
else
  SELECTOR_STR=""
fi

cmd="ungrouped"
case ${FORMAT} in
  "cve")
    FORMAT_STR="${CVE_ONLY_FORMAT}"
    cmd="ungrouped"
    ;;
  "cve_grouped")
    FORMAT_STR="${CVE_ONLY_FORMAT}"
    cmd="grouped_by_details"
    ;;
  "packages_grouped")
    FORMAT_STR="${CVE_ONLY_FORMAT}"
    cmd="grouped_by_packages"
    ;;
  "long")
    FORMAT_STR="${LONG_FORMAT}"
    HEADER_STR="${LONG_HEADER}"
    cmd="ungrouped"
    ;;
  "terse")
    FORMAT_STR="${CVE_TERSE_FORMAT}"
    HEADER_STR=""
    cmd="ungrouped"
    ;;
  *)
    echo "!!! Unknown format, defaulting to ungrouped listing"
    FORMAT_STR="${LONG_FORMAT}"
    HEADER_STR="${LONG_HEADER}"
    ;;
esac

$cmd "$SELECTOR_STR" $@

