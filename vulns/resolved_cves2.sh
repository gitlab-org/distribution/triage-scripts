#!/bin/bash -e

# CLI overrides the environment
OLD_SCAN=${1:-${OLD_SCAN}}
NEW_SCAN=${2:-${NEW_SCAN}}

# SCOPE=${SCOPE:-"os"}
PROJECT=${PROJECT:-"gitlab-org/build/CNG"}
DEFAULT_OS=${DEFAULT_OS:-"rhel8"}
GLAB=${GLAB:-"echo -- glab"}
GLAB_RO=${GLAB_RO:-${GLAB}}
GLAB_RW=${GLAB_RW:-${GLAB}}
MESSAGE=${MESSAGE:-""}

COMMENTED_ON=${COMMENTED_ON}

_my_path=$(realpath $0)
_my_dir=$(dirname $_my_path)
OS_TRANSLATE=${OS_TRANSLATE:-"${_my_dir}/os_translate.sh"}
removed=$(${_my_dir}/compare_scans.sh ${OLD_SCAN} ${NEW_SCAN})

_oses=$(jq -r '[.vulnerabilities[].location.operating_system ] | unique | .[] | @sh' "${NEW_SCAN}" | xargs -n1 ${OS_TRANSLATE} | uniq)
if [ $(echo "$_oses" | wc -l) -ne 1 ]; then
  echo "There should be exactly one OS, instead I've got:"
  echo ""
  echo "$_oses"
  echo ""
  echo "You may want to run os_splitter.sh to separate results"
  exit 1
else
  REPORT_OS="${_oses}"
fi

REPORT_OS=${REPORT_OS:-${DEFAULT_OS}}

echo "Resolved CVES:"
echo "${removed}"

${_my_dir}/compare_scans.sh ${OLD_SCAN} ${NEW_SCAN} | while read cve_item
do
  cve=$(echo ${cve_item}| awk 'BEGIN{FS="|"}{print $2;}' )
  scope=$(echo ${cve_item}| awk 'BEGIN{FS="|"}{print $1;}' )
  os=${REPORT_OS}
  image=$(echo ${cve_item}| awk 'BEGIN{FS="|"}{print $3;}' )
  target=$(echo ${cve_item}| awk 'BEGIN{FS="|"}{print $4;}' )
  sanitized_target=${target##*/}
  echo "==> $cve $scope"
  existing_issues=""
  if [ "${scope}" == "os" ]; then
    existing_issues=$($GLAB_RO issue -R ${PROJECT} list -A --in title --search "$cve" -l "os::${os}" | awk -e '/^#/ {sub("^#","",$1); print $1}')
    # existing_issues=$($GLAB_RO issue -R ${PROJECT} list -A --in title --search "$cve" | awk -e '/^#/ {sub("^#","",$1); print $1}')
  else
    _located_issues=$(${GLAB_RO} issue -R ${PROJECT} list -A --in title --search "$cve" | awk -e '/^#/ {sub("^#","",$1); print $1}')
    if [ -n "${_located_issues}" ] ; then
        for _issue in ${_located_issues}
        do
            _issue_title=$(${GLAB_RO} issue view -R ${PROJECT} ${_issue}| awk '/^title:/ { $1=""; print $0}')
            if echo "${_issue_title}" | grep -qF "${sanitized_target}"; then
                existing_issues="$existing_issues ${_issue}"
            fi
        done
    fi
  fi

  if [ -n "${existing_issues}" ]; then
      # Existing issues found
      echo "--> Found existing issues: ${existing_issues}"
      for issue in ${existing_issues}
      do
          # glab issue -R ${PROJECT} view ${issue}
          if [ "${scope}" == "os" ]; then
              # OS
              _note=$(echo -e "$cve no longer detected within ${image}. \n\n${MESSAGE}")
          else
              # Application, go, gem
              _note=$(echo -e "$cve no longer detected in ${sanitized_target} within ${image}. \n\n${MESSAGE}")
          fi
          $GLAB_RW issue note -R ${PROJECT} -m "${_note}" ${issue} | tee -a ${COMMENTED_ON}
      done
  else
    echo "No existing issues found for $cve"
  fi

done