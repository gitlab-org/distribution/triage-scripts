#!/bin/bash

SCAN_FILE=${SCAN_FILE:-"${HOME}/tmp/gitlab-container-registry-v3.57.0-fips.json"}
LABELS=${LABELS:-""}
PROJECT=${PROJECT:-"gitlab-org/build/CNG"}
SCOPE=${SCOPE:-"all"}

GLAB=${GLAB:-"echo -e -- glab"}

_my_path=$(realpath $0)
_my_dir=$(dirname $_my_path)

CVE_MD=${CVE_MD:-"cve.md"}
FORMAT="cve_grouped" SCOPE="${SCOPE}" ${_my_dir}/issue_table.sh $SCAN_FILE > ${CVE_MD}

for cve in $@
do
    _labels=""
    if [ -n ${LABELS} ]; then
        _labels="-l \"${LABELS}\""
    else
        _labels=""
    fi
    issues=$(${GLAB} issue list -R ${PROJECT} -A --in title --search "$cve" ${_labels} | awk -e '/^#/ {sub("^#","",$1); print $1}')
    for _issue in $issues
    do
        echo "* [$cve](https://gitlab.com/${PROJECT}/-/issues/${_issue})"
    done
    if [ -z "${issues}" ]; then
        echo "* $cve issue has not been filed"
        grep -F $cve ${CVE_MD} | sed -e 's#^#  #g'
    fi
done