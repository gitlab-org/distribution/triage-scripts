#!/bin/env python

import sys
import json
import re
import os
from collections import UserDict
from typing import override
import jsondiff

src_vm=sys.argv[1]
src_tso=sys.argv[2]
data_dir=os.environ.get('DATA_DIR','_data')
prefix=''

issues_vm=[]
issues_tso=[]

class VulerabilityDict(UserDict):
    def __init__(self):
        self.data={}
        self.lookup={}
        self.errors=[]
    @override
    def __setitem__(self, key: str, item: dict[str, str]) -> None:
        # item: {"vuln_id":, "image":, "items":, "issue":}
        if key not in self.data:
            self.data[key]={}
            self.lookup[key]={}
        for image in item['images'].split(','):
            (image_name, image_tag) = image.rsplit(':',2)
            if image_name.startswith('gitlab-org'):
                image_name='registry.gitlab.com/'+image_name
            elif image_name.startswith('ubi'):
                image_name='registry.access.redhat.com/'+image_name
            if image_name not in self.data[key]:
                try:
                    self.data[key][image_name]=[]
                    self.lookup[key][image_name]=[]
                except KeyError as ke:
                    print("Failed to unwrap {}: {}".format(key, item))
                    raise ke
            try:
                items=item['items'].split(',')
                self.data[key][image_name].extend(items)
                self.lookup[key][image_name].append(item['issue'])
            except KeyError as ke:
                print("Failed to unwrap {}: {}".format(key, item))
                raise ke

class IssuesReader(object):
    issues_map : VulerabilityDict
    errors : list

    def __init__(self):
        self.issues_map=VulerabilityDict()

    def read(self, filename):
        pass

    def get_map(self):
        ##FIXME: very ugly hack to sneaker errors around
        self.issues_map.errors=self.errors
        return self.issues_map

class TSOReader(IssuesReader):
    def __init__(self, tso_filename):
        super().__init__()
        # CVE-2024-34064 detected in rust-srpm-macros of gitlab-org/build/cng/gitlab-workhorse-ee:master-fips
        self.tso_title_re=re.compile(r'^(?P<vuln_id>\S+)\s+detected in (?P<items>\S+)(?: of (?P<images>\S+))?$')
        self.tso_description_re=re.compile(r'.*found in `(?P<image>\S+?)`.*')

        self.errors=[]
        self.read(tso_filename)

    @override
    def read(self, filename):
        tso_title_re=self.tso_title_re
        tso_map=self.issues_map
        tso_description_re=self.tso_description_re
        with open(filename, "r") as tso_issues_file:
            issues_tso = json.load(tso_issues_file)

        for tso_issue in issues_tso:
            tso_match=tso_title_re.match(tso_issue['title'])
            if tso_match:
                tso_dict=tso_match.groupdict()
            else:
                print("* Couldn't parse `{}`".format(tso_issue['title']), file=sys.stderr)
                self.errors.append(tso_issue)
                continue
            if 'images' not in tso_dict or not tso_dict['images']:
                try:
                    tso_dict['images']=tso_description_re.match(tso_issue['description']).groupdict()['image']
                except AttributeError:
                    print("Failed to extract image name from description {} {}".format(tso_dict['vuln_id'], tso_issue['description']))
                    self.errors.append(tso_issue)
            # print(tso_dict)
            tso_dict['issue']=tso_issue['web_url']
            try:
                tso_map[tso_dict["vuln_id"]]=tso_dict
            except Exception:
                self.errors.append(tso_issue)
                print(json.dumps(tso_dict), file=sys.stderr)

class VMReader(IssuesReader):
    def __init__(self, filename):
        super().__init__()
        # CVE-2024-2961 detected in image registry.gitlab.com/gitlab-org/build/cng/gitlab-container-registry:master-fips, package glibc-minimal-langpack
        self.vm_title_re=re.compile(r'^(?P<vuln_id>\S+)\s+detected in image (?P<images>\S+), package (?P<items>\S+)$')

        self.errors=[]
        self.read(filename)

    @override
    def read(self, filename):
        with open(filename, "r") as src_vm_file:
            issues_vm = json.load(src_vm_file)
        for vm_issue in issues_vm:
            vm_match=self.vm_title_re.match(vm_issue['title'])
            try:
                vm_dict=vm_match.groupdict()
                vm_dict['issue']=vm_issue['web_url']
                self.issues_map[vm_dict['vuln_id']]=vm_dict
            except AttributeError as ae:
                print("* Couldn't parse `{}`".format(vm_issue['title']), file=sys.stderr)
                self.errors.append(vm_issue)

def print_cve_diff(tso_map, vm_map):
    for vid in tso_map:
        if vid in vm_map:
            print("   {}".format(vid))
        else:
            print("tso {}".format(vid))

    for vid in vm_map:
        if vid in tso_map:
            # print("   {}".format(vid))
            pass
        else:
            print("vm {}".format(vid))

def save_maps(tso_map, vm_map):
    with open(data_dir+'/'+prefix+'tso_map.json','w') as tso_output:
        print(json.dumps(tso_map.data), file=tso_output)
        print(tso_output)
    with open(data_dir+'/'+prefix+'vm_map.json','w') as vm_output:
        print(json.dumps(vm_map.data), file=vm_output)
        print(vm_output)

def save_lookups(tso_map, vm_map):
    with open(data_dir+'/'+prefix+'tso_lookup.json','w') as tso_lookup:
        print(json.dumps(tso_map.lookup), file=tso_lookup)
        print(tso_lookup)
    with open(data_dir+'/'+prefix+'vm_lookup.json','w') as vm_lookup:
        print(json.dumps(vm_map.lookup), file=vm_lookup)
        print(vm_lookup)

def save_errors(tso_map, vm_map):
    with open(data_dir+'/'+prefix+'tso_errors.json','w') as tso_output:
        print(json.dumps(tso_map.errors), file=tso_output)
        print(tso_output)
    with open(data_dir+'/'+prefix+'vm_errors.json','w') as vm_output:
        print(json.dumps(vm_map.errors), file=vm_output)
        print(vm_output)

def map_diff(tso_map, vm_map):
    diff=jsondiff.diff(tso_map.data, vm_map.data, dump=True, syntax="explicit")
    print(diff)

cmds={
    'cve-diff': print_cve_diff,
    'save-maps': save_maps,
    'save-errors': save_errors,
    'save-lookups': save_lookups,
    'diff': map_diff
}

tsor=TSOReader(src_tso)
tso_map=tsor.get_map()

vmr=VMReader(src_vm)
vm_map=vmr.get_map()

for cmd_name in sys.argv[3:]:
    try:
        cmd=cmds[cmd_name]
        cmd(tso_map, vm_map)
    except KeyError:
        if cmd_name.startswith('prefix='):
            prefix=cmd_name.removeprefix('prefix=')
    
