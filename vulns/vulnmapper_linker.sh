#!/bin/bash

TSO_LOOKUP=${TSO_LOOKUP:-_data3/tso_lookup.json}
VM_LOOKUP=${VM_LOOKUP:-_data3/vm_lookup.json}

parsed_issue(){
  # https://gitlab.com/gitlab-org/build/CNG/-/issues/801
  local issue_url=$1
  local issue_id=${issue_url##*/}
  local project_url=${issue_url%%"/-/issues/${issue_id}"}
  local no_proto_url=${project_url##*://}
  local issue_hostname=${no_proto_url%%/*}
  local issue_project=${no_proto_url##"${issue_hostname}"}
  local project_id=$(echo "\"${issue_project}\"" | jq -r '@uri "\(.)"')
  echo "${project_id}|${issue_id}"
}

get_pid(){
  local _parsed_issue="$1"
  echo "${_parsed_issue}" | cut -d"|" -f1
}

get_iid(){
  local _parsed_issue="$1"
  echo "${_parsed_issue}" | cut -d"|" -f2
}


for _cve in $(jq -r '. | keys | .[]' "${TSO_LOOKUP}" )
do
  tso_issues=$(jq -r --arg cve ${_cve} '.[$cve] | select(.) | .[] | .[]' "${TSO_LOOKUP}")
  vm_issues=$(jq -r --arg cve ${_cve} '.[$cve] | select(.) | .[] | .[]' "${VM_LOOKUP}")
  echo "${_cve}"
  for ti in ${tso_issues}
  do 
    parsed_ti=$(parsed_issue "$ti")
    source_pid=$(get_pid "${parsed_ti}")
    source_iid=$(get_iid "${parsed_ti}")
    [ -n "${vm_issues}" ] && for vi in ${vm_issues}
    do
      if [ "$ti" == "$vi" ]; then
        continue
      fi
      parsed_vi=$(parsed_issue "$vi")
      target_pid=$(get_pid "${parsed_vi}")
      target_iid=$(get_iid "${parsed_vi}")
      echo glab api \
          -X POST \
          /projects/${source_pid}/issues/${source_iid}/links \
          -F target_project_id=${target_pid} \
          -F target_issue_iid=${target_iid}
    done
  done
done
