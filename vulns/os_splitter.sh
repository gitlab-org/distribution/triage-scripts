#!/bin/bash

SCAN_FILE=${1}
DEST_DIR=${2:-${DEST_DIR:-"."}}

_oses=$(jq -r '[.vulnerabilities[].location.operating_system ] | unique | join("|")' ${SCAN_FILE})

mkdir -p "${DEST_DIR}"

old_IFS=$IFS 
IFS="|"
for os in ${_oses}
do
    jq --arg os "${os}" '.vulnerabilities |= [.[] | select(.location.operating_system==$os)]' ${SCAN_FILE} > ${DEST_DIR}/"${os}".json
done
IFS=$old_IFS