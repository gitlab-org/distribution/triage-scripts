#!/bin/bash

OS_GUESS="debian"

if echo "$1" | grep -qF 'Red Hat Enterprise Linux release 8'; then
    OS_GUESS="rhel8"
fi

echo ${OS_GUESS}