#!/bin/bash


GLAB=${GLAB:-glab}
TSO_PROJECT=${TSO_PROJECT:-dmakovey/cng-tso}
VM_PROJECT=${VM_PROGECT:-gitlab-org/build/cng-security-test}
MERGED_PROJECT=${MERGED_PROJECT:-${TSO_PROJECT}}
DATA_DIR=${DATA_DIR:-_data}

_TSO_PROJECT=$(echo "\"${TSO_PROJECT}\"" | jq -r '@uri "\(.)"')
_VM_PROJECT=$(echo "\"${VM_PROJECT}\"" | jq -r '@uri "\(.)"')
_MERGED_PROJECT=$(echo "\"${MERGED_PROJECT}\"" | jq -r '@uri "\(.)"')

TSO_LABEL=${TSO_LABEL:-TSO}
VM_LABEL=${VM_LABEL:-vulnmapper}

COMMON_LABELS=${COMMON_LABELS:-FedRAMP::Vulnerability,security}

fetch(){
  ${GLAB} api \
    --paginate \
    -X GET \
    -F labels=${COMMON_LABELS} \
    -F state=opened \
    "/projects/${_TSO_PROJECT}/issues" \
    | jq -n '[inputs|.[]]' \
    > ${DATA_DIR}/tso_issues.json
  ${GLAB} api \
    --paginate \
    -X GET \
    -F labels=${COMMON_LABELS} \
    -F state=opened \
    "/projects/${_VM_PROJECT}/issues" \
    | jq -n '[inputs|.[]]' \
    > ${DATA_DIR}/vm_issues.json
}

fetch_merged(){
  ${GLAB} api \
    --paginate \
    -X GET \
    -F labels=${TSO_LABEL} \
    -F state=opened \
    "/projects/${_MERGED_PROJECT}/issues" \
    | jq -n '[inputs|.[]]' \
    > ${DATA_DIR}/merged_tso_issues.json
  ${GLAB} api \
    --paginate \
    -X GET \
    -F labels=${VM_LABEL} \
    -F "not[labels]=${TSO_LABEL}" \
    -F state=opened \
    "/projects/${_MERGED_PROJECT}/issues" \
    | jq -n '[inputs|.[]]' \
    > ${DATA_DIR}/merged_vm_issues.json
}


diff(){
  ./vulnmapper_diff.py ${DATA_DIR}/tso_issues.json ${DATA_DIR}/vm_issues.json diff
}

if [ "$#" -ge 1 ]; then
  for cmd in "$@"
  do
    ${cmd}
  done
else
  fetch
  diff
fi
