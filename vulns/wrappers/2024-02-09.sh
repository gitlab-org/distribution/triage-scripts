#!/bin/bash

RUN=${RUN:-test1}
set -e

setup() {
	set -a
	PREVIOUS_SCAN_TIMESTAMP="2024-02-01"
	PREVIOUS_SCAN_URL="https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/pipelines/1098334206"
	PREVIOUS_SCAN_TRIGGER_URL="https://gitlab.com/gitlab-org/build/CNG/-/pipelines/1098294519"
	SCAN_TIMESTAMP="2024-02-09"
	SCAN_URL="https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/pipelines/1138127202"
	SCAN_TRIGGER_URL="https://gitlab.com/gitlab-org/build/CNG/-/pipelines/1165718790"
	MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n"

	LABELS="group::distribution::build,FedRAMP,FedRAMP::Vulnerability,devops::systems,group::distribution,bug::vulnerability,section::enablement,security,type::bug"
	DATA_DIR=_data/${SCAN_TIMESTAMP}
	PREVIOUS_DATA_DIR=_data/${PREVIOUS_SCAN_TIMESTAMP}
	set +a
}

fetch_scans() {
	mkdir -p _data/{${PREVIOUS_SCAN_TIMESTAMP},${SCAN_TIMESTAMP}}
	[ -r _data/${SCAN_TIMESTAMP}/gl-container-scanning-report.json ] ||
		vulns/get_scan.sh ${SCAN_URL} >_data/${SCAN_TIMESTAMP}/gl-container-scanning-report.json
	[ -r _data/${PREVIOUS_SCAN_TIMESTAMP}/gl-container-scanning-report.json ] ||
		vulns/get_scan.sh ${PREVIOUS_SCAN_URL} >_data/${PREVIOUS_SCAN_TIMESTAMP}/gl-container-scanning-report.json
	# SCAN_FILE=_data/${SCAN_TIMESTAMP}/gl-container-scanning-report.json
}

# OS:

prep_os() {
	FORMAT="terse" \
		SCOPE="os" \
		bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${DATA_DIR}/os_cve_list.txt
	FORMAT="cve_grouped" \
		SCOPE="os" \
		bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${DATA_DIR}/os_cve_list.grouped.txt

	FORMAT="terse" \
		SCOPE="os" \
		bash vulns/issue_table.sh ${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${PREVIOUS_DATA_DIR}/os_cve_list.txt
	FORMAT="cve_grouped" \
		SCOPE="os" \
		bash vulns/issue_table.sh ${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${PREVIOUS_DATA_DIR}/os_cve_list.grouped.txt
	# preemptively fetch CSAF DB avoiding stderr capturing
	vulns/rh_cve_response.sh fetch
}

diff_os() {
	comm -13 ${PREVIOUS_DATA_DIR}/os_cve_list.grouped.txt ${DATA_DIR}/os_cve_list.grouped.txt
}

run_os() {
	SCAN_FILE=${DATA_DIR}/gl-container-scanning-report.json \
		COMMENT_ON_DUPLICATE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
		COMMENTED_ON=${DATA_DIR}/os_commented_on.${RUN}.txt \
		CREATED_ISSUES=${DATA_DIR}/os_created_issues.${RUN}.txt \
		LABELS="${LABELS}" \
		GLAB_RO="glab" \
		MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
		PROJECT="gitlab-org/build/CNG" \
		bash vulns/cve_issue.sh $(cat ${DATA_DIR}/os_cve_list.txt) \
			2>> ${DATA_DIR}/os_error.${RUN}.log
	echo "==> Errors encountered:"
	cat ${DATA_DIR}/os_error.${RUN}.log
}

run_os_adhoc() {
	SCAN_FILE=${DATA_DIR}/gl-container-scanning-report.json \
		COMMENT_ON_DUPLICATE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
		COMMENTED_ON=${DATA_DIR}/os_commented_on.${RUN}.txt \
		CREATED_ISSUES=${DATA_DIR}/os_created_issues.${RUN}.txt \
		LABELS="${LABELS}" \
		GLAB_RO="glab" \
		MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
		PROJECT="gitlab-org/build/CNG" \
		bash vulns/cve_issue.sh $CVES \
			2>> ${DATA_DIR}/os_error.${RUN}.log
	echo "==> Errors encountered:"
	cat ${DATA_DIR}/os_error.${RUN}.log
}

# App:

prep_app() {
	FORMAT="terse" \
		SCOPE="application go python gem" \
		bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${DATA_DIR}/app_cve_list.txt
	FORMAT="cve_grouped" \
		SCOPE="application go python gem" \
		bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${DATA_DIR}/app_cve_list.grouped.txt

	FORMAT="terse" \
		SCOPE="application go python gem" \
		bash vulns/issue_table.sh ${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${PREVIOUS_DATA_DIR}/app_cve_list.txt
	FORMAT="cve_grouped" \
		SCOPE="application go python gem" \
		bash vulns/issue_table.sh ${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json |
		sort -u \
			>${PREVIOUS_DATA_DIR}/app_cve_list.grouped.txt
}

diff_app() {
	comm -13 ${PREVIOUS_DATA_DIR}/app_cve_list.grouped.txt ${DATA_DIR}/app_cve_list.grouped.txt
}

run_app() {
	SCAN_FILE=${DATA_DIR}/gl-container-scanning-report.json \
		COMMENT_ON_DUPLICATE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
		COMMENTED_ON=${DATA_DIR}/app_commented_on.${RUN}.txt \
		CREATED_ISSUES=${DATA_DIR}/app_created_issues.${RUN}.txt \
		LABELS="${LABELS}" \
		GLAB_RO="glab" \
		MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
		PROJECT="gitlab-org/build/CNG" \
		bash vulns/app_cve_issue.sh $(cat ${DATA_DIR}/app_cve_list.txt) \
			2>>${DATA_DIR}/app_error.${RUN}.log
	echo "==> Errors encountered:"
	cat ${DATA_DIR}/app_error.${RUN}.log
}

run_app_adhoc() {
	SCAN_FILE=${DATA_DIR}/gl-container-scanning-report.json \
		COMMENT_ON_DUPLICATE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
		COMMENTED_ON=${DATA_DIR}/app_commented_on.${RUN}.txt \
		CREATED_ISSUES=${DATA_DIR}/app_created_issues.${RUN}.txt \
		LABELS="${LABELS}" \
		GLAB_RO="glab" \
		MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
		PROJECT="gitlab-org/build/CNG" \
		bash vulns/app_cve_issue.sh $CVES \
			2>>${DATA_DIR}/app_error.${RUN}.log
	echo "==> Errors encountered:"
	cat ${DATA_DIR}/app_error.${RUN}.log
}

preview() {
	setup
	fetch_scans
	prep_os
	(echo "OS CVEs:"; diff_os) | less -K
	run_os
	prep_app
	(echo "App CVEs:"; diff_app) | less -K
	run_app
}

process() {
	setup
	export GLAB_RW="glab"
	fetch_scans
	prep_os
	(echo "OS CVEs:"; diff_os) | less -K
	run_os
	prep_app
	(echo "App CVEs:"; diff_app) | less -K
	run_app
}

resolve() {
	GLAB_RO=glab \
		MESSAGE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
		COMMENTED_ON=${DATA_DIR}/resolved_commented_on.${RUN}.txt \
		bash vulns/resolved_cves2.sh \
		${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json \
		${DATA_DIR}/gl-container-scanning-report.json
}

[ $# -lt 1 ] && exit 1

for cmd in "$@"; do
	$cmd
done

