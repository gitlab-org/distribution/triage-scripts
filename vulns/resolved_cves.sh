#!/bin/bash -e

# CLI overrides the environment
OLD_SCAN=${1:-${OLD_SCAN}}
NEW_SCAN=${2:-${NEW_SCAN}}

SCOPE=${SCOPE:-"os"}
GLAB=${GLAB:-"echo glab --"}
GLAB_RO=${GLAB_RO:-${GLAB}}
GLAB_RW=${GLAB_RW:-${GLAB}}
MESSAGE=${MESSAGE:-""}

OLD_CVES=$(FORMAT="terse" SCOPE=${SCOPE} bash vulns/issue_table.sh ${OLD_SCAN} | sort -u)
NEW_CVES=$(FORMAT="terse" SCOPE=${SCOPE} bash vulns/issue_table.sh ${NEW_SCAN} | sort -u)

removed=$(comm -23 <(echo ${OLD_CVES} | xargs -n 1 echo) <(echo ${NEW_CVES} | xargs -n 1 echo) )

echo "Removed CVES:"
echo "${removed}"

for cve in ${removed}
do
  if [ "${SCOPE}" == "os" ]; then
    # existing_issues=$($GLAB_RO issue -R ${PROJECT} list -A --in title --search "$cve" -l "os::${_os}" | awk -e '/^#/ {sub("^#","",$1); print $1}')
    existing_issues=$($GLAB_RO issue -R ${PROJECT} list -A --in title --search "$cve" | awk -e '/^#/ {sub("^#","",$1); print $1}')
  else
    existing_issues=$($GLAB_RO issue -R ${PROJECT} list -A --in title --search "$cve" | awk -e '/^#/ {sub("^#","",$1); print $1}')
  fi
  cve_entries=$(jq --arg cve "$cve" -r '[.vulnerabilities[] | select(.identifiers[].name == $cve)]' ${OLD_SCAN})
  if [ "${cve_entries}" == "[]" ]; then
    echo "=ERROR=> Can't find $cve mention in ${OLD_SCAN}"
  else
    _last_entry=$(echo "${cve_entries}" | jq '. | length - 1 ')
    # echo "Counted entries: $_last_entry"
    for _index in $(seq 0 ${_last_entry})
    do
        # echo "Processing entry 0"
        _cve_entry=$(echo "${cve_entries}" | jq ".[${_index}]")
        _cve_field="$(echo ${_cve_entry} | jq -r .cve)"
        _cve_category=${_cve_field##*:}
        if [ -n "${existing_issues}" ]; then
            # Existing issues found
            echo "--> Found existing issues: ${existing_issues}"
            for issue in ${existing_issues}
            do
                # glab issue -R ${PROJECT} view ${issue}
                if [ "${_cve_category}" == "os" ]; then
                    # OS
                    _note=$(echo -e "$cve no longer detected. \n\n${MESSAGE}")
                else
                    # Application, go, gem
                    _note=$(echo -e "$cve no longer detected. \n\n${MESSAGE}")
                fi
                $GLAB_RW issue note -R ${ISSUE_PROJECT} -m "${_note}" ${issue} | tee -a ${COMMENTED_ON}
            done
        else 
          echo "No existing issues found for $cve"
        fi
    done
  fi
done