#!/bin/bash

# Assumptions:
#   * input files should list **single** OS
#   * we're using firse CVE entry under `identifiers` as a key

{

# Application CVEs
# We're not filtering out "os" items as they filter themselves out as their "path" attribute is empty
jq  -r '.vulnerabilities[] | {"cve_type": (.cve | gsub(".*:";"")), "names": [.identifiers[].name], "name": .identifiers[0].name, "path": (.location.dependency.package.path|split(";")), "package_name": .location.dependency.package.name, "version": .location.dependency.version, "image": .location.image, "short_image": (.location.image | sub(".*?/";"")), "os_long": .location.operating_system } | [["\(.cve_type)|\(.name)|\(.short_image)|\(.os_long)"],.path] | combinations | join("|") ' ${1}


# OS CVEs
jq -r '[.vulnerabilities[] | {"cve": .identifiers[0].name, "cve_type": (.cve | gsub(".*:";"")), "package_name": .location.dependency.package.name, "image": .location.image, "short_image": (.location.image | sub(".*?/";"")), "os_long": .location.operating_system } | select(.cve_type == "os") | {"cve_key": "\(.cve)|\(.short_image)|\(.os_long)", "package_name": .package_name}] |group_by(.cve_key) | .[] | { "cve_key": .[0].cve_key, "packages": ([.[].package_name] |unique | join(",")) } | "os|\(.cve_key)|\(.packages)"' ${1}

} | sort