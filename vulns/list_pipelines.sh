#!/bin/bash

DATA_DIR=${DATA_DIR:-_data}
FROM_DATE=${FROM_DATE:-$(date "+%Y-%m-%d" --date="week ago")}

mkdir -p ${DATA_DIR}

get_all_pipelines(){
    glab api -X GET --paginate \
        -F updated_after=${FROM_DATE} \
        -F source="schedule" \
        -F ref=master \
        -F status=success \
        /projects/gitlab-org%2Fbuild%2FCNG/pipelines \
    | jq -r '.[].id' \
    > ${DATA_DIR}/pipelines.txt
}

filter_pipelines(){
    for p in $(cat ${DATA_DIR}/pipelines.txt)
    do
        glab ci get -R gitlab-org/build/CNG -p ${p} -F json > _data/${p}.json
    done

    for pf in $(grep -lF 'container-scanning' ${DATA_DIR}/*.json)
    do
        jq -r .id ${pf}
    done > ${DATA_DIR}/pipelines_filtered.txt
}


extract_references(){
    for p in $(cat ${DATA_DIR}/pipelines_filtered.txt)
    do
        glab ci get -R gitlab-org/build/CNG -p ${p} -F json | jq '.jobs[] | select(.name=="container-scanning") | {"pipeline": .pipeline.id, "project": .pipeline.project_id, "job": .id, "started_at": .started_at}'
    done | jq -rs '. | sort_by("started_at") | .[] |"/projects/\(.project)/jobs/\(.job)/trace|\(.pipeline)"' > ${DATA_DIR}/paths.txt

    for t in $(cat ${DATA_DIR}/paths.txt)
    do
        pipeline_id=${t##*|}
        endpoint=${t%|*}
        glab api "${endpoint}" \
          | awk 'BEGIN{print_me=0} /curl/ {print_me=1} /Cleaning up/ { print_me=0;} print_me==1 { print; }' \
          | sed 's#^[^{]*##g; s#[^}]*$##g' \
          | tail -n 1 | jq  --arg pipeline_id "${pipeline_id}" '. | {"created": .created_at, "pipeline_url": .web_url, "triggered_by": ("https://gitlab.com/gitlab-org/build/CNG/-/pipelines/"+$pipeline_id)}'
    done | jq -s '. | sort_by(.created)'
}

get_all_pipelines
filter_pipelines
extract_references