#!/bin/bash

DOCKER=${DOCKER:-"podman"}
DOCKERARGS=${DOCKERARGS}

IMAGE=${1:-${IMAGE}}
PACKAGE=${2:-${PACKAGE}}

rhel() {
	${DOCKER} run ${DOCKERARGS} --rm --user=root --entrypoint="" -it ${IMAGE} bash -c "if command -v dnf > /dev/null; then dnf repoquery -q ${PACKAGE}; else microdnf makecache > /dev/null 2>&1; microdnf repoquery ${PACKAGE} 2>/dev/null; fi"
}

debian() {
	${DOCKER} run ${DOCKERARGS} --rm --user=root --entrypoint="" -it ${IMAGE} sh -c "apt-get update > /dev/null 2>&1; apt-cache show ${PACKAGE} | awk '/^Version:/ { print \"${PACKAGE}-\"\$2}'"
}

distro_check() {
	OS_ID=$(${DOCKER} run ${DOCKERARGS} --rm --entrypoint="" -it ${IMAGE} sh -c ". /etc/os-release; echo \$ID" | tr -d '\r')
	echo ${OS_ID}
}

distro=$(distro_check)

$distro
