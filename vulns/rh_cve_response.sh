#!/bin/bash

set -e
YQ=${YQ:-yq}
FORMAT=${FORMAT:-"yaml"}

RH_CSAF_ADVISORIES_URL="https://access.redhat.com/security/data/csaf/v2/advisories"
RH_CSAF_ADVISORIES_DIR=${RH_CSAF_ADVISORIES_DIR:-"_data/redhat-csaf"}
RH_CSAF_ADVISORIES_ARCHIVE=${RH_CSAF_ADVISORIES_ARCHIVE:-"csaf_advisories.tar.zst"}

fetch() {
	local _advisories_archive
	local _advisory_files
	# local _real_archive
	_advisories_archive=$(curl -Ls ${RH_CSAF_ADVISORIES_URL}/archive_latest.txt)
	curl -Lo "${RH_CSAF_ADVISORIES_ARCHIVE}" "${RH_CSAF_ADVISORIES_URL}/${_advisories_archive}"
	# _real_archive=$(realpath ${RH_CSAF_ADVISORIES_FILE})

	mkdir -p "${RH_CSAF_ADVISORIES_DIR}"
	if [ -r ${RH_CSAF_ADVISORIES_DIR}/index.cur ]; then
		echo "Found existing index" >&2
		mv ${RH_CSAF_ADVISORIES_DIR}/index.cur ${RH_CSAF_ADVISORIES_DIR}/index.prev
	fi
	tar --zstd -xf "${RH_CSAF_ADVISORIES_ARCHIVE}" -C "${RH_CSAF_ADVISORIES_DIR}"
	(
		cd ${RH_CSAF_ADVISORIES_DIR}
		find . -name \*.json | sort
	) >${RH_CSAF_ADVISORIES_DIR}/index.cur

	if [ -r ${RH_CSAF_ADVISORIES_DIR}/index.prev ]; then
		_advisory_files=$(comm -31 ${RH_CSAF_ADVISORIES_DIR}/index.{cur,prev})
	else
		_advisory_files=$(find ${RH_CSAF_ADVISORIES_DIR} -name \*.json)
	fi

    echo "Indexing RedHat CSAF DB" >&2 
	for adv in ${_advisory_files}; do
	 	echo -n "." >&2
		for cve in $(jq -r '.vulnerabilities[].cve' $adv 2>>${RH_CSAF_ADVISORIES_DIR}/pivot.log); do
			echo "$cve $adv"
		done
	done >${RH_CSAF_ADVISORIES_DIR}/pivot.txt
}

conditional_fetch(){
	if [ ! -r "${RH_CSAF_ADVISORIES_DIR}/pivot.txt" ]; then
		# no pivot data
		fetch
	elif [ $(($(date '+%s') - $(stat -c '%Y' ${RH_CSAF_ADVISORIES_DIR}/pivot.txt))) -gt 604800 ]; then
		# data is older than 7 days
		rm -rf ${RH_CSAF_ADVISORIES_DIR}
		fetch
	fi

}

report() {
	local _cve=$1
	local _advisory_file
	conditional_fetch
	_advisory_files=$(grep -F "$_cve" ${RH_CSAF_ADVISORIES_DIR}/pivot.txt | cut -f2 -d" ")
	for af in ${_advisory_files}; do
		_cpes=$(jq -r '.product_tree.branches | .. | .product_identification_helper? // empty | .cpe? // empty' ${af})
		_rhel=false
		for _c in ${_cpes}; do
			if [[ "${_c:0:30}" == 'cpe:/o:redhat:enterprise_linux' ]]; then
				_rhel=true
			fi
		done
		if [[ "${_rhel}" == 'true' ]]; then
			# we only care for RHEL entries
			_statement=$(jq -r '.vulnerabilities[].notes[] | select( .title == "Statement") | .text' ${af})
			_remediations=$(jq -r '.vulnerabilities[].remediations[].details' ${af})

			if [ "${FORMAT}" == "yaml" ]; then
				echo "# ${af}"
				echo "vendor:"
				if [ -n "${_statement}" ]; then
					echo ""
					echo "  statement: |"
					echo "${_statement}" | sed -e 's#^#    #'
				fi
				if [ -n "${_remediations}" ]; then
					echo ""
					echo "  mitigation: |"
					echo "${_remediations}" | sed -e 's#^#    #'
				fi
			elif [ "${FORMAT}" == "md" ]; then
				echo "## Vendor"
				if [ -n "${_statement}" ]; then
					echo ""
					echo "### Statement"
					echo "${_statement}"
				fi
				if [ -n "${_remediations}" ]; then
					echo ""
					echo "### Mitigation"
					echo "${_remediations}"
				fi
			fi
		fi
	done
}

# fetch
if [[ "$1" == "fetch" ]]; then
	conditional_fetch
else
	report $1
fi