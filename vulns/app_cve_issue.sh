#!/bin/bash -e

SCAN_FILE=${SCAN_FILE:-"${HOME}/tmp/gitlab-container-registry-v3.57.0-fips.json"}
LABELS=${LABELS:-""}
MESSAGE=${MESSAGE:-""}
PROJECT=${PROJECT:-"gitlab-org/build/CNG"}
SEARCH_PROJECT=${SEARCH_PROJECT:-${PROJECT}}
ISSUE_PROJECT=${ISSUE_PROJECT:-${PROJECT}}
COMMENT_ON_DUPLICATE=${COMMENT_ON_DUPLICATE:-""}
REGISTRY_PATH=${REGISTRY_PATH:-"registry.gitlab.com/gitlab-org/build/cng/"}
DEFAULT_OS=${DEFAULT_OS:-"rhel8"}
CONFIDENTIAL=${CONFIDENTIAL:-"-c"}

EXCLUDE_BINARIES=${EXCLUDE_BINARIES:-""}

CREATED_ISSUES=${CREATED_ISSUES:-"/dev/null"}
COMMENTED_ON=${COMMENTED_ON:-"/dev/null"}

GLAB=${GLAB:-"echo -e -- glab"}
GLAB_RW=${GLAB_RW:-${GLAB}}
GLAB_RO=${GLAB_RO:-${GLAB}}
_my_path=$(realpath $0)
_my_dir=$(dirname $_my_path)

for cve in "$@"
do
  echo "=> Processing $cve"
  cve_entries=$(jq --arg cve "$cve" -r '[.vulnerabilities[] | select(.identifiers[].name == $cve)]' ${SCAN_FILE})
  if [ "${cve_entries}" == "[]" ]; then
    echo "=ERROR=> Can't find $cve mention in $SCAN_FILE"
  else
    _last_entry=$(echo "${cve_entries}" | jq '. | length - 1 ')
    # echo "Counted entries: $_last_entry"
    for _index in $(seq 0 ${_last_entry})
    do
        # echo "Processing entry 0"
        _cve_entry=$(echo "${cve_entries}" | jq ".[${_index}]")
        # echo ${_cve_entry}
        image_uri=$(echo ${_cve_entry} | jq -r '.location.image' )
        _severity="$(echo ${_cve_entry} | jq -r '.severity')"
        _cve_field="$(echo ${_cve_entry} | jq -r .cve)"
        _cve_category=${_cve_field##*:}
        _package_name=$(echo ${_cve_entry} | jq -r '.location.dependency.package.name' )
        _package_path=$(echo ${_cve_entry} | jq -r '.location.dependency.package.path' )
        _package_version=$(echo ${_cve_entry} | jq -r '.location.dependency.version' )

        _cve_link=$(echo ${_cve_entry} | jq -r '[.identifiers[]| "[\(.name)](\(.url))" ] | join(",")')
        _cve_links=$(echo ${_cve_entry} | jq -r '[ .identifiers[] | "[\(.name)](\(.url))" ] | join(",")')
        _cve_links_list=$(echo ${_cve_entry} | jq -r '[ .identifiers[] | "* [\(.name)](\(.url))" ] | join("\\n")')
        _cve_description=$(echo ${_cve_entry} | jq -r .description)
        _cve_details=$(echo ${_cve_entry} | jq -r .details)
        if [ "${_cve_details}" != "null" ]; then
            _details_text=$(echo $_cve_details | jq -r '. | [to_entries[] | if .value.type=="list" then ([.value.name, (.value.items | map("  * "+.name+" "+.value) | join("\n") )] | "* \(.[0])\n\(.[1])") else ("* "+.value.name + " " + .value.value) end] | join("\n")')
        else
            _details_text=""
        fi
        _image_name=$(echo ${image_uri} | sed -e "s#${REGISTRY_PATH}##g")

        existing_issues=""
        if [ "$_cve_category" == "os" ]; then
            # OS
            echo "Skipping OS-level CVE $cve" >&2 
        else
            # application, go, gem
            _listing="."
            case "$_cve_category" in
                application|go)
                    _listing='[.location.dependency.package.path | split(";")] |flatten | unique | .[] | sub(".*/"; "")'
                    ;;
                gem|python)
                    _listing='.location.dependency.package.name'
                    ;;
                *) 
                    echo "Unknown CVE category: '${_cve_category}'" >&2
                    exit 1
                    ;;
            esac
            for _item in $(echo "${_cve_entry}" | jq -r "${_listing}")
            do
                _located_issues=$(${GLAB_RO} issue -R ${SEARCH_PROJECT} list --in title --search "$cve $_item" | awk -e '/^#/ {sub("^#","",$1); print $1}')
                if [ -n "${_located_issues}" ] ; then
                    for _issue in ${_located_issues}
                    do
                        _issue_title=$(${GLAB_RO} issue view -R ${SEARCH_PROJECT} ${_issue}| awk '/^title:/ { $1=""; print $0}')
                        if echo "${_issue_title}" | grep -qF "${_item}"; then
                            existing_issues="$existing_issues ${_issue}"
                        fi
                    done
                fi
            done
        fi
        if [ -n "${existing_issues}" ]; then
            # Existing issues found
            echo "--> Found existing issues: ${existing_issues}"
            for issue in ${existing_issues}
            do
                # glab issue -R ${PROJECT} view ${issue}
                if [ -n "${COMMENT_ON_DUPLICATE}" ]; then
                if [ "${_cve_category}" == "os" ]; then
                    # OS
                    echo "Skipping OS-level CVE $cve" >&2 
                else
                    # Application, go, gem
                    _binaries=$(echo "${_cve_entry}" | jq -r '[.location.dependency.package.path | split(";")] | flatten | unique | join(", ")')
                    _note=$(echo -e "$cve detected in \`$image_uri\` for \`${_package_name}-${_package_version}\` found in \`${_binaries}\`\n\n${COMMENT_ON_DUPLICATE}")
                fi
                $GLAB_RW issue note -R ${ISSUE_PROJECT} -m "${_note}" ${issue} | tee -a ${COMMENTED_ON}
                fi
            done
        else
            # No existing issues found

            _severity_level=""
            case $_severity in
            Critical)
              _severity_level="1"
              ;;
            High)
              _severity_level="2"
              ;;
            Medium)
              _severity_level="3"
              ;;
            Low)
              _severity_level="4"
              ;;
            esac

            #TODO we may need to use `glab api` call if description becomes too big and we cannot pass it via CLI

            if [ "${_cve_category}" == "os" ]; then
                # OS
                echo "Skipping OS-level CVE $cve" >&2 
            else
                # Application, go, gem, Python
                _binary_list=$(echo "${_cve_entry}" | jq -r '[.location.dependency.package.path | split(";") ] | flatten | unique | join(" ")')
                # we file CVE:binary issue
                echo "--> Binaries found: $_binary_list"
                if [ -z "$_binary_list" ]; then echo "${_cve_entry}" | jq .; fi
                for _binary in ${_binary_list}
                do
                    _binary_name=${_binary##*/}
                    if echo "$EXCLUDE_BINARIES" | grep -Fq "${_binary_name}"; then
                        echo "${_binary_name} is excluded"
                        continue
                    fi
                    _pkg_info=""
                    if [ "${_cve_category}" == "application" ]; then
                        _pkg_info="for package ${_package_name}-${_package_version}"
                    elif [ "${_cve_category}" == "os" ]; then
                        _pkg_info="for package ${_package_name}-${_package_version}"
                    fi

                    # Since we key issues differently for non-OS instances we need to check for existing isues:
                    _located_issues_pre=$(${GLAB_RO} issue -R ${SEARCH_PROJECT} list -A --in title --search "$cve $_binary_name" | awk -e '/^#/ {sub("^#","",$1); print $1}')
                    _located_issues=""
                    if [ -n "${_located_issues_pre}" ] ; then
                        for _issue in ${_located_issues_pre}
                        do
                            _issue_title=$(${GLAB_RO} issue view -R ${SEARCH_PROJECT} ${_issue}| awk '/^title:/ { $1=""; print $0}')
                            if echo "${_issue_title}" | grep -qF "${_binary_name}"; then
                                _located_issues="${_located_issues} ${_issue}"
                            fi
                        done
                    fi
                    if [ -z "${_located_issues}" ] ; then
                        _labels="${LABELS}"
                        if [ -n "${_severity_level}" ]; then
                          _labels="${_labels},priority::${_severity_level},severity::${_severity_level}"
                        fi

                        # create new issue
                        _issue_description=$(echo -e "${_cve_link} found in \`${image_uri}\` \`${_pkg_info}\` binary \`${_binary}\`\n\n${_cve_description}\n\n${_details_text}\n\n${MESSAGE}")
                        $GLAB_RW issue create -R ${ISSUE_PROJECT} ${CONFIDENTIAL} -t "$cve detected in $_binary_name" --label "$_labels" -d "${_issue_description}" | tee -a ${CREATED_ISSUES}
                    else
                        # add comment to existing issue
                        for _issue in ${_located_issues}
                        do
                            if [ -n "${COMMENT_ON_DUPLICATE}" ]; then
                                _binaries=$(echo "${_cve_entry}" | jq -r '[.location.dependency.package.path | split(";")] | flatten | unique | join(", ")')
                                _note=$(echo -e "$cve detected in \`${image_uri}\` for \`${_package_name}-${_package_version}\` found in \`${_binaries}\`\n\n${COMMENT_ON_DUPLICATE}")
                                $GLAB_RW issue note -R ${ISSUE_PROJECT} -m "${_note}" ${_issue} | tee -a ${COMMENTED_ON}
                            fi
                        done
                    fi

                done
            fi
        fi
    done
  fi

done