#!/bin/bash -e

SCAN_FILE=${SCAN_FILE:-"${HOME}/tmp/gitlab-container-registry-v3.57.0-fips.json"}
LABELS=${LABELS:-""}
MESSAGE=${MESSAGE:-""}

PROJECT=${PROJECT:-"gitlab-org/build/CNG"}
SEARCH_PROJECT=${SEARCH_PROJECT:-${PROJECT}}
ISSUE_PROJECT=${ISSUE_PROJECT:-${PROJECT}}

COMMENT_ON_DUPLICATE=${COMMENT_ON_DUPLICATE:-""}
REGISTRY_PATH=${REGISTRY_PATH:-"registry.gitlab.com/gitlab-org/build/cng/"}
DEFAULT_OS=${DEFAULT_OS:-"rhel8"}
CONFIDENTIAL=${CONFIDENTIAL:-"-c"}

CREATED_ISSUES=${CREATED_ISSUES:-"/dev/null"}
COMMENTED_ON=${COMMENTED_ON:-"/dev/null"}

GLAB=${GLAB:-"echo -e -- glab"}
GLAB_RO=${GLAB_RO:-$GLAB}
GLAB_RW=${GLAB_RW:-$GLAB}
_my_path=$(realpath $0)
_my_dir=$(dirname $_my_path)
OS_TRANSLATE=${OS_TRANSLATE:-"${_my_dir}/os_translate.sh"}
export DOCKER=docker

_oses=$(jq -r '[.vulnerabilities[].location.operating_system ] | unique | .[] | @sh' "${SCAN_FILE}" | xargs -n1 ${OS_TRANSLATE} | uniq)
if [ $(echo "$_oses" | wc -l) -ne 1 ]; then
	echo "There should be exactly one OS, instead I've got:"
	echo ""
	echo "$_oses"
	echo ""
	echo "You may want to run os_splitter.sh to separate results"
	exit 1
else
	REPORT_OS="${_oses}"
fi

REPORT_OS=${REPORT_OS:-${DEFAULT_OS}}

for cve in "$@"; do
	cve_entries=$(jq --arg cve "$cve" -r '[.vulnerabilities[] | select(.identifiers[].name == $cve)]' "${SCAN_FILE}")
	if [ "${cve_entries}" == "[]" ]; then
		echo "=ERROR=> Can't find $cve mention in $SCAN_FILE"
	else

		packagelist=$(echo "${cve_entries}" | jq -r '[.[] | .location.dependency.package.name] | unique | map("`\(.)`") | join(", ")')
		compact_packagelist=$(echo "${cve_entries}" | jq -r '[.[] | .location.dependency.package.name] | unique | map("\(.)") | join(",")')
		detailed_packagelist=$(echo "${cve_entries}" | jq -r '[.[] | .location.dependency | "\(.package.name)-\(.version)"] | unique | map("`\(.)`") | join(", ")')
		some_cve_entry=$(echo "${cve_entries}" | jq -r '.[0]')
		image_uri=$(echo ${some_cve_entry} | jq -r '.location.image')
		image_list=$(echo ${cve_entries} | jq -r '[.[].location.image ] | unique | map("`\(.)`") | join(", ")')

		_severity="$(echo ${some_cve_entry} | jq -r '.severity')"

		## Here's how to extract category from existing report:
		_cve_field="$(echo $some_cve_entry | jq -r .cve)"
		_cve_category=${_cve_field##*:}

		_cve_link="$(echo $some_cve_entry | jq -r '[.identifiers[]| "[\(.name)](\(.url))" ] | join(",")')"
		_cve_links="$(echo $some_cve_entry | jq -r '[ .identifiers[] | "[\(.name)](\(.url))" ] | join(",")')"
		_cve_links_list="$(echo $some_cve_entry | jq -r '[ .identifiers[] | "* [\(.name)](\(.url))" ] | join("\\n")')"
		_cve_description=$(echo $some_cve_entry | jq -r .description)
		_cve_details=$(echo ${some_cve_entry} | jq -r .details)
		if [ "${_cve_details}" != "null" ]; then
			_details_text=$(echo $_cve_details | jq -r '. | [to_entries[] | if .value.type=="list" then ([.value.name, (.value.items | map("  * "+.name+" "+.value) | join("\n") )] | "* \(.[0])\n\(.[1])") else ("* "+.value.name + " " + .value.value) end] | join("\n")')
		else
			_details_text=""
		fi
		_os=${REPORT_OS}
		_image_name=$(echo ${image_uri} | sed -e "s#${REGISTRY_PATH}##g")
		_image_path=${image_uri#*/}
		existing_issues=""
		if [ "$_cve_category" == "os" ]; then
			# OS
			existing_issues=$($GLAB_RO issue -R ${SEARCH_PROJECT} list -A --in title --search "$cve" -l "os::${_os}" | awk -e '/^#/ {sub("^#","",$1); print $1}')
		else
			# application, go, gem
			##XXX _listing="."
			##XXX case "$_cve_category" in
			##XXX application | go)
			##XXX 	_listing='[.[] | .location.dependency.package.path | split(";")] |flatten | unique | .[] | sub(".*/"; "")'
			##XXX 	;;
			##XXX gem | python)
			##XXX 	_listing='[.[] | .location.dependency.package.name] | unique | .[]'
			##XXX 	;;
			##XXX *) 
			##XXX  	echo "Unknown CVE category: '${_cve_category}'"
			##XXX 	exit 1
			##XXX 	;;
			##XXX esac
			##XXX for _item in $(echo "${cve_entries}" | jq -r "${_listing}"); do
			##XXX 	_located_issues=$($GLAB_RO issue -R ${SEARCH_PROJECT} list -A --in title --search "$cve $_item" | awk -e '/^#/ {sub("^#","",$1); print $1}')
			##XXX 	if [ -n "${_located_issues}" ]; then
			##XXX 		existing_issues="$existing_issues ${_located_issues}"
			##XXX 	fi
			##XXX done
			echo "Skipping non-OS findings for $cve"
			continue
		fi
		_issue_title="$cve detected in ${compact_packagelist} of ${_image_path}"
		available_packages=""
		_vendor_response=""
		for _pkg in $(echo "${cve_entries}" | jq -r '[.[] | .location.dependency.package.name] | unique | .[]'); do
			_pkg_list=$(${_my_dir}/check_available.sh ${image_uri} ${_pkg})
			_pkg_available=""
			for _p in ${_pkg_list}; do
				_pkg_available="* ${_p}\n${_pkg_available}"
			done
			available_packages="${available_packages}\n\n${_pkg_available}"
		done

		if [ -n "${existing_issues}" ]; then
			# Existing issues found
			echo "Found existing issues: ${existing_issues}"
			for issue in ${existing_issues}; do
				if [ -n "${COMMENT_ON_DUPLICATE}" ]; then

					if [ -n "${available_packages}" ]; then
						_available_msg="### Currently available packages ${available_packages}"
						_vendor_response="${_vendor_response}\n\n${_available_msg}"
					else
						_available_msg=""
					fi

					if [ "${_cve_category}" == "os" ]; then
						# OS
						_note=$(echo -e "$cve detected in $image_list for $packagelist\n\n${_available_msg}\n\n${COMMENT_ON_DUPLICATE}")
					else
						# Application, go, gem
						##XXX _binaries=$(echo "${cve_entries}" | jq -r '[.[] | .location.dependency.package.path | split(";")] | flatten | unique | map("`\(.)`") | join(", ")')
						##XXX _note=$(echo -e "$cve detected in \`$image_list\` for $detailed_packagelist found in ${_binaries}\n\n${COMMENT_ON_DUPLICATE}")
						echo "Skipping non-OS findings for $cve"
						continue
					fi
					$GLAB_RW issue note -R ${ISSUE_PROJECT} -m "${_note}" ${issue} | tee -a ${COMMENTED_ON}
				fi
				if [ "${UPDATE_TITLE}" == "yes" ]; then
					echo -n "Existing issue "
					$GLAB_RO issue view -R ${SEARCH_PROJECT} ${issue} ${CONFIDENTIAL} | grep -e '^title:'
					echo "Changing to: ${_issue_title}"
					$GLAB_RW issue update -R ${ISSUE_PROJECT} ${issue} -t "${_issue_title}" | tee -a ${CREATED_ISSUES}
				fi
			done
		else
			# No existing issues found

			_severity_level=""
			case $_severity in
			Critical)
				_severity_level="1"
				;;
			High)
				_severity_level="2"
				;;
			Medium)
				_severity_level="3"
				;;
			Low)
				_severity_level="4"
				;;
			esac

			#TODO we may need to use `glab api` call if description becomes too big and we cannot pass it via CLI

			if [ "${_cve_category}" == "os" ]; then
				# OS
				if echo ${REPORT_OS} | grep -qe "^rhel"; then
					export YQ
					_vendor_response=$(FORMAT="md" ${_my_dir}/rh_cve_response.sh ${cve})
				fi

				if [ -n "${available_packages}" ]; then
					_available_msg="## Currently available packages ${available_packages}"
					_vendor_response="${_vendor_response}\n\n${_available_msg}"
				fi

				_labels="os::${_os}"

				if [ -n "${_severity_level}" ]; then
					_labels="${_labels},priority::${_severity_level},severity::${_severity_level}"
				fi

				if [ -n "${LABELS}" ]; then
					_labels="${_labels},${LABELS}"
				fi

				_issue_description=$(echo -e "${_cve_link} found in \`${image_uri}\` for package(s) \`$packagelist\` \n\n${_cve_description}\n\n${_details_text}\n\n${_vendor_response}\n\n${MESSAGE}")
				$GLAB_RW issue create -R ${ISSUE_PROJECT} ${CONFIDENTIAL} -t "${_issue_title}" --label "$_labels" -d "${_issue_description}" | tee -a ${CREATED_ISSUES}
			else
				# Application, go, gem, python, etc.
				echo "Skipping non-OS findings for $cve"
			fi
		fi

	fi
done
