# Vulnerability reporting helper scripts

## Requirements

* yq
* jq
* gawk
* glab
* container runtime (one of):
  * docker
  * podman

## References

* Security response API from RH (for example: https://access.redhat.com/hydra/rest/securitydata/cve/CVE-2021-43267.json )
* NIST API ( https://services.nvd.nist.gov/rest/json/cves/2.0?cveId=CVE-2022-2182 )
* RH Bugzilla responses ( https://access.redhat.com/security/data/metrics/cve-metadata-from-bugzilla.xml )

## Usage (simlified with wrapper)

Open new issue for the scan analysis under https://gitlab.com/gitlab-org/distribution/triage-scripts/-/issues . From here on - record processing and any issues encountered under that issue for future references.

Query previously ran pipelines for available scans:

```shell
FROM_DATE=$(date --date "3 days ago" +%Y-%m-%d) vulns/list_pipelines.sh > _data/scans.json
```

From `_data/scans.json` select "previous" and "current" scans to be processed along with pipeline URLs.


Get a copy of latest wrapper script from `wrappers/` directory as a timestamped filename, like `2024-01-07.sh` (based on the date of execution for ease of location later)

Modify `2024-01-07.sh` header to point to proper dates/pipelines as present in `_data/scans.json`:

```shell
PREVIOUS_SCAN_TIMESTAMP="2023-12-07"
PREVIOUS_SCAN_URL="https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/pipelines/1098334206"
PREVIOUS_SCAN_TRIGGER_URL="https://gitlab.com/gitlab-org/build/CNG/-/pipelines/1098294519"
SCAN_TIMESTAMP="2024-01-15"
SCAN_URL="https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/pipelines/1138127202"
SCAN_TRIGGER_URL="https://gitlab.com/gitlab-org/build/CNG/-/pipelines/1138087526"
```

Make sure:

* `GITLAB_TOKEN` env variable is set with PAT for access to GitLab instance
* `docker` or `podman` (depending on which one is being used) are logged into the GitLab registry (`docker login registry.gitlab.....`)
* Run wrapper script from the repo root directory

At this point script is ready to run:

```shell
RUN="test1" ./2024-01-07.sh preview
```

this will only do a "preview" run displaying proposed changes and differences between previous scan and current. `diff_*()` commands are being redirected to `less` for convenience to be able to see difference before moving forward and allowing for process termination: `^C` - will terminate the entire script run, whereas `:q` would continue processing.

After outcomes from `preview` have been analyzed (inspect `_data/*/*.txt` files) we can launch real processing based on the same dataset that `preview` was operating on:

```shell
RUN="final" GLAB_RW="glab" ./2024-01-07.sh setup run_os run_app
```

at this point all issues should be filed/modified appropriately.

Finally run `resolve` helper to point out CVEs that no longer appear in scans to aid in issue closure:

```shell
RUN="final" GLAB_RW=glab bash -x 2024-01-07.sh setup resolve
```

Once everything has been filed if there were any major changes that needed to be preserved for `2024-01-07.sh` - commit it under `wrappers/` directory. Otherwise feel free to discard.

## Usage

Report every finding from the scan (skip duplicates):

```shell
SCAN_FILE="gitlab-container-registry-v3.57.0-ubi.json" vulns/cve_issue.sh $(FORMAT="terse" SCOPE="go" bash vulns/issue_table.sh gitlab-container-registry-v3.57.0-ubi.json | sort -u)
```

Report every finding from the scan (comment on duplicates):

```shell
SCAN_FILE="gitlab-container-registry-v3.57.0-ubi.json" COMMENT_ON_DUPLICATES="detected on rescan" vulns/cve_issue.sh $(FORMAT="terse" SCOPE="go" bash vulns/issue_table.sh gitlab-container-registry-v3.57.0-ubi.json | sort -u)
```

## Locating past scan pipelines

```shell
mkdir -p _data
FROM_DATE=$(date --date "14 days ago")
```

fetch list of all suitable pipelines that ran within last two weeks (use different date as needed)

```shell
FROM_DATE=${FROM_DATE} vulns/list_pipelines.sh
```

## OS CVE filing Process

1. Create `os::*` labels necessary for the execution
2. collect all the CVEs:
   ```shell
   FORMAT="terse" SCOPE="os" bash vulns/issue_table.sh gitlab-container-registry-v3.57.0-ubi.json | sort -u > os_cve_list.txt
   ```
3. using `cve_grouped` format remove from `cve_list.txt` all non-OS entries
   ```shell
   FORMAT="cve_grouped" SCOPE="os" bash vulns/issue_table.sh gitlab-container-registry-v3.57.0-ubi.json | sort -u > os_cve_list_grouped.txt
   ```
4. File all the CVE issues:
   ```shell
   SCAN_FILE="gitlab-container-registry-v3.57.0-ubi.json"\
     COMMENT_ON_DUPLICATE="detected on rescan" \
     COMMENTED_ON="os_commented_on.txt" \
     CREATED_ISSUES="os_created_issues.txt" \
     LABELS="group::distribution::build,FedRAMP,FedRAMP::Vulnerability,devops::systems,group::distribution,bug::vulnerability,section::enablement,security,type::bug" \
     GLAB="glab" \
     MESSAGE="Scan origin: http://gitlab.com/some/path/to/scan_results" \
     bash -x vulns/cve_issue.sh $(cat os_cve_list.txt)
   ```
   **NOTE**: there are `jq` errors popping up when `bash -x` is not used.

   **NOTE**: `MESSAGE` needs to be updated to reflect scan origin

### Rollback of actions

Issues are easy to remove:

```shell
GLAB="glab" CREATED_ISSUES="os_created_issues.txt" COMMENTED_ON="os_commented_on.txt" vulns/rollback.sh issues
```

Notes:

```shell
GLAB="glab" CREATED_ISSUES="os_created_issues.txt" COMMENTED_ON="os_commented_on.txt" vulns/rollback.sh notes
```

or both:

```shell
GLAB="glab" CREATED_ISSUES="os_created_issues.txt" COMMENTED_ON="os_commented_on.txt" vulns/rollback.sh notes issues
```


## Application/Golang CVE filing process

1. Set up initial variables:
   ```shell
   SCAN_FILE="gitlab-container-registry-v3.57.0-ubi.json"
   SCAN_URL="http://gitlab.com/path/to/scan/pipeline"
   ```
2. collect all the CVEs:
   ```shell
   FORMAT="terse" SCOPE="application go" bash vulns/issue_table.sh ${SCAN_FILE} | sort -u > app_cve_list.txt
   ```
3. using `cve_grouped` format remove from `cve_list.txt` all non-OS entries
   ```shell
   FORMAT="cve_grouped" SCOPE="application go" bash vulns/issue_table.sh ${SCAN_FILE} | sort -u > app_cve_list_grouped.txt
   ```
4. File all the CVE issues:
   ```shell
   SCAN_FILE=${SCAN_FILE} \
     COMMENT_ON_DUPLICATE="detected on rescan" \
     COMMENTED_ON="os_commented_on.txt" \
     CREATED_ISSUES="os_created_issues.txt" \
     LABELS="group::distribution::build,FedRAMP,FedRAMP::Vulnerability,devops::systems,group::distribution,bug::vulnerability,section::enablement,security,type::bug" \
     GLAB="glab" \
     MESSAGE="Scan origin: ${SCAN_URL}" \
     bash -x vulns/cve_issue.sh $(cat app_cve_list.txt)
   ```

   ```shell
   PROJECT="dmakovey/my-tests" GLAB="glab" \
    SCAN_FILE=${SCAN_FILE} \
    COMMENT_ON_DUPLICATE="\n\nScan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
    CREATED_ISSUES="app_created_issues.txt" \
    COMMENTED_ON="app_commented_on.txt" \
    LABELS="group::distribution::build,FedRAMP,FedRAMP::Vulnerability,devops::systems,group::distribution,bug::vulnerability,section::enablement,security,type::bug" \
    MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
    vulns/app_cve_issue.sh $(cat app_cve_list.txt)
   ```

   **NOTE**: `MESSAGE` may need to be updated to reflect scan origin

## Backlinks generation

Generating list of issues created from specified scan can be performed using `backlinks.sh`:

```shell
SCAN_FILE="_data/20220823/gl-container-scanning-report-620296667.json" \
  GLAB="glab" \
  CVE_MD="_data/20220823/620296667-cve.md" \
  vulns/backlinks.sh $(FORMAT="terse" SCOPE="not_go" vulns/issue_table.sh _data/20220823/gl-container-scanning-report-620296667.json) \
  | uniq \
  | tee _data/20220823/620296667-baclinks.md
```

# 2023-10-25

Before start:

```shell
LABELS="group::distribution::build,FedRAMP,FedRAMP::Vulnerability,devops::systems,group::distribution,bug::vulnerability,section::enablement,security,type::bug"
DATA_DIR=_data/20231025
PREVIOUS_DATA_DIR=_data/20231011
mkdir -p ${DATA_DIR} ${PREVIOUS_DATA_DIR}
# lets store Bugzilla export along with scans
export RH_CVE_BUGZILLA_FILE=${DATA_DIR}/rh_bugzilla.xml
# fetch most current URIs for pipelines and store in ${DATA_DIR}/urls
SCAN_URL="https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/pipelines/1048491767/security"
SCAN_TRIGGER_URL="https://gitlab.com/gitlab-org/build/CNG/-/jobs/5368510255"
```

If you are using a M1/2 MAC, also run

```shell
DOCKERARGS="--platform=linux/amd64"
```

Copy/Download the latest scan result to the `${DATA_DIR}/gl-container-scanning-report.json` file.

Copy/Download the previous scan result from one of app issue's latest comment to the `${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json` file.

## OS

```shell
FORMAT="terse" \
   SCOPE="os" \
   bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json \
   | sort -u \
   > ${DATA_DIR}/os_cve_list.txt
FORMAT="cve_grouped" \
   SCOPE="os" \
   bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json \
   | sort -u \
   > ${DATA_DIR}/os_cve_list.grouped.txt

# Preview new CVEs
comm -13 ${PREVIOUS_DATA_DIR}/os_cve_list.grouped.txt ${DATA_DIR}/os_cve_list.grouped.txt

RUN=test-1 \
   SCAN_FILE=${DATA_DIR}/gl-container-scanning-report.json \
   COMMENT_ON_DUPLICATE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
   COMMENTED_ON=${DATA_DIR}/os_commented_on.${RUN}.txt \
   CREATED_ISSUES=${DATA_DIR}/os_created_issues.${RUN}.txt \
   LABELS="${LABELS}" \
   GLAB_RO="glab" \
   MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
   PROJECT="gitlab-org/build/CNG" \
   bash vulns/cve_issue.sh $(cat ${DATA_DIR}/os_cve_list.txt)
```

Re-run previous command with `RUN=final-test-` and `GLAB_RW="glab"` for posting updates in issues

## App

```shell
FORMAT="terse" \
   SCOPE="application go" \
   bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json \
   | sort -u \
   > ${DATA_DIR}/app_cve_list.txt
FORMAT="cve_grouped" \
   SCOPE="application go" 
   bash vulns/issue_table.sh ${DATA_DIR}/gl-container-scanning-report.json \
   | sort -u \
   > ${DATA_DIR}/app_cve_list.grouped.txt

# Preview new CVEs
comm -13 ${PREVIOUS_DATA_DIR}/app_cve_list.grouped.txt ${DATA_DIR}/app_cve_list.grouped.txt

RUN=test-1 \
   SCAN_FILE=${DATA_DIR}/gl-container-scanning-report.json \
   COMMENT_ON_DUPLICATE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
   COMMENTED_ON=${DATA_DIR}/app_commented_on.${RUN}.txt \
   CREATED_ISSUES=${DATA_DIR}/app_created_issues.${RUN}.txt \
   LABELS="${LABELS}" \
   GLAB_RO="glab" \
   MESSAGE="Scan URL: ${SCAN_URL}\n\nScan Trigger URL: ${SCAN_TRIGGER_URL}\n" \
   PROJECT="gitlab-org/build/CNG" \
   bash vulns/app_cve_issue.sh $(cat ${DATA_DIR}/app_cve_list.txt)
```

Re-run previous command with `RUN=final-test-` and `GLAB_RW="glab"` for posting updates in issues

## Resolved

```shell
RUN=test-1 \
   GLAB_RO=glab \
   MESSAGE="\n\nScan: ${SCAN_URL}\n\nScan triggered by: ${SCAN_TRIGGER_URL}\n" \
   COMMENTED_ON=${DATA_DIR}/resolved_commented_on.${RUN}.txt \
   bash vulns/resolved_cves2.sh \
      ${PREVIOUS_DATA_DIR}/gl-container-scanning-report.json \
      ${DATA_DIR}/gl-container-scanning-report.json
```

Re-run previous command with `RUN=final-test-` and `GLAB_RW="glab"` for posting updates in issues
