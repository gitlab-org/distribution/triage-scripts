#!/bin/sh

CREATED_ISSUES=${CREATED_ISSUES:-"created_issues.txt"}
COMMENTED_ON=${COMMENTED_ON:-"commented_on.txt"}
GLAB=${GLAB:-"echo -- glab"}

issues(){
    # echo "Could delete an issue"
    # exit 0
    if [ -r "${CREATED_ISSUES}" ]; then
        cat "${CREATED_ISSUES}" | xargs -n1 ${GLAB} issue delete 
    else
        echo "No ${CREATED_ISSUES}"
    fi
}


notes(){
    local uri_path
    local project_path
    local issue_note
    local issue_iid
    local note_id
    local project_path_encoded
    if [ -r "${COMMENTED_ON}" ]; then
        for url in $(cat ${COMMENTED_ON})
        do
            uri_path=${url##https://gitlab.com/}
            project_path=${uri_path%%/-/*}
            issue_note=${uri_path#*/-/issues/}
            issue_iid=${issue_note%"#note"*}
            note_id=${issue_note#*note_}
            project_path_encoded=$(echo "\"${project_path}\"" | jq -r '@uri "\(.)"')
            echo "Deleting note $url"
            ${GLAB} api -X DELETE /projects/${project_path_encoded}/issues/${issue_iid}/notes/${note_id}
        done
    else
        echo "No ${COMMENTED_ON}"
    fi
}

if [ $# -lt 1 ]; then
    notes
    issues
else
    for cmd in "$@" 
    do
        $cmd
    done
fi