#!/bin/bash

OLD_SCAN=${1:-${OLD_SCAN}}
NEW_SCAN=${2:-${NEW_SCAN}}

comm -23 \
  <(vulns/cve_map.sh ${OLD_SCAN} | cut -d'|' -f1,2,3,5 | sort) \
  <(vulns/cve_map.sh ${NEW_SCAN} | cut -d'|' -f1,2,3,5 | sort)